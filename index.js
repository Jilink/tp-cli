#!/usr/bin/env node
const program = require("commander")
const inquirer = require("inquirer")
const fs = require("fs")
const axios = require('axios')
var opn = require('opn'); // to open links
const random = require('random')
const say = require('say')
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(':memory:');
const Meme = require('./meme')






let currentMemes = [] // memes selected from db



// DB CREATION

let memes = new sqlite3.Database("./memes.db", 
    sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, 
    (err) => { 
      if (err) { 
        console.log('Error when creating the database', err) 
    } else { 
        createTable()
    } 
    });


const createTable = () => {
  memes.run("CREATE TABLE IF NOT EXISTS saved(id INTEGER PRIMARY KEY AUTOINCREMENT,sub TEXT, name TEXT, url TEXT)");
}

const insertData = (sub,name,url) =>{
  console.log(`Insert data`)
  memes.run(`INSERT INTO saved (sub, name, url) VALUES ("${sub}","${name}","${url}")`);
}

const selectFromName = (name) => {

  memes.all(`SELECT * FROM saved WHERE name="${name}" `, [], (err, rows) => {
    if (err) {
      throw err;
    }
    console.log(name)

    const meme = new Meme(rows[0].sub,rows[0].name,rows[0].url)

      currentMemes.push(meme)
      displayHTML(currentMemes)

  });
}

const selectFromSub = (sub) => {

  memes.all(`SELECT * FROM saved WHERE sub="${sub}" `, [], (err, rows) => {
    if (err) {
      throw err;
    }
    rows.forEach((row) => {
      const meme = new Meme(row.sub,row.name,row.url)

      currentMemes.push(meme)
    });
    displayHTML(currentMemes)


  });
}

const selectAll = () => {

  memes.all(`SELECT * FROM saved `, [], (err, rows) => {
    if (err) {
      throw err;
    }
    rows.forEach((row) => {
      const meme = new Meme(row.sub,row.name,row.url)

      currentMemes.push(meme)
    });
    displayHTML(currentMemes)


  });
}


    // PROGRAME :

program
  .version("1.0.0")
  .option('-n --noura','noura')
  .option('-s --subreddit [name]','subreddit')
  .option('-f --filtre [filtre]','filtre')
  .option('-d --database [name]','database') // accéder les memes sauvegardés en fonction du nom ou du subreddit
  .option('-a --all','all') // pour accéder à tous les memes sauvegardés 





program.parse(process.argv)
// les différentes option
if (program.subreddit && program.filtre) { // precising subreddit and filtre
  axios.get(`https://www.reddit.com/r/${program.subreddit}/${program.filtre}.json?limit=100`)
  .then((reponse) => {
    let index 
    index = random.int( min = 0, max = 99)
    const randoimg = reponse.data.data.children[index].data.url
    say.speak(`voici une image de ${program.subreddit}`)
    opn(randoimg)
    return sqlSave(program.subreddit,randoimg)

  })
  .catch((eror) => { // precising subreddits
    say.speak(`Erreur il n'existe pas de page  ${program.subreddit} ou de filtre ${program.filtre}`)

    console.log(eror)
  })
}   else if (program.database && program.subreddit ){ // calling saved memes by sub name
  console.log(`accessing saved meme from subreddit ${program.subreddit}`)
  selectFromSub(program.subreddit)
  
}  else if (program.database && program.all ){ // calling all saved memes
  console.log(`accessing all saved memes `)
  selectAll()
  
} 
else if (program.subreddit) {
  axios.get(`https://www.reddit.com/r/${program.subreddit}/hot.json?limit=100`)
  .then((reponse) => {
    let index
    index = random.int( min = 0, max = 99)
    const randoimg = reponse.data.data.children[index].data.url
    say.speak(`voici une image de ${program.subreddit}`)

    opn(randoimg)
    return sqlSave(program.subreddit,randoimg)

  })
  .catch((eror) => {
    say.speak(`Erreur il n'existe pas de page  ${program.subreddit}`)

    console.log(eror)
  })

} else if(program.noura){
  axios.get('https://www.reddit.com/r/hmmm/hot.json?limit=100')
  .then((reponse) => {
    let index
    index = random.int( min = 0, max = 99)
    const firsthot = reponse.data.data.children[index].data.url
    say.speak("ispice di shaloum")
    opn(firsthot)
    return sqlSave("hmmm",firsthot)

  })
  .catch((eror) => {
    console.log(eror)
  })

} else if (program.database){ // calling saved memes name
  console.log("accessing meme")
  selectFromName(program.database)
  
}
else { // calling reddit with no option
  axios.get('https://www.reddit.com/r/hmmm/hot.json?limit=100')
  .then((reponse) => {
    let index 
    index = random.int( min = 0, max = 99)
    const randoimg = reponse.data.data.children[index].data.url
    opn(randoimg)
    return sqlSave("hmmm",randoimg)
  })
  .catch((eror) => {
    console.log(eror)
  })
}


function sqlSave(sub,url) {
  inquirer.prompt([
        {
          message: 'sauvegarder ? (y/n)',
          name: 'img'
        }
    
      ])
      .then((answers) => {
        console.log(answers.img)
        let answer= answers.img
        if(answer === 'y') {
          let memeName ="undefined name"
          console.log(url)
          inquirer.prompt([
            {
              message: 'Parfait, quel nom donner au meme ?',
              name: 'name'
            }
          ])
          .then((answers) => {
            memeName = "" + answers.name
            insertData(sub,memeName,url)

          })
        }
        // memes.close()
      })
      .catch((eror) => {
        console.log(eror)
      })
  


}
















// FICHIERS Ecriture dans html :

function displayHTML(currentMemes) {

  let allImg = ""
  for (meme of currentMemes) {
    // <p> </p> <p> ${meme.name} </p>
    
    allImg += `<div class="card w-100 " style="width: 18rem;">
    <img  class="card-img-top" src="${meme.url}" alt="${meme.name}">
    <div class="card-body bg-white">
    <ul class="list-group list-group-flush">
      <li class="list-group-item bg-light text-black"> <h3> Subreddit : </h3> <span class = "font-italic" > ${meme.sub} </span></li> 
      <li class="list-group-item bg-light text-black"> <h3> Name : </h3> <span class = "font-italic" > ${meme.name} </span> </li>
    </ul>

    </div>
  </div>`
  }

  // console.log(allImg)
  
  try{
    console.log("Creating html content")
    fs.writeFile('index.html',`<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <title>Document</title>
    </head>
    <body class="bg-white">
 
    <div class="container offset-md-2 col-md-8">
    ${allImg}

    </div>
    
      
    </body>
    </html>`

    , (eror => {
      if(eror) throw eror
    }))

    opn('index.html')
  } catch {

  }
  
}
